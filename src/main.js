import Vue from 'vue'
import Vuetify from 'vuetify'
import VueResource from 'vue-resource'
import _ from 'lodash'
import './stylus/main.styl'
import App from './App'
import router from './router'
import { store } from './store'
import DateFilter from './filters/date'
import * as firebase from 'firebase'
import AlertCmp from '../src/components/Shared/Alert.vue'
import EditNewsDetailsDialog from '../src/components/news/edit/editNewsDetailsDialog.vue'
import EditNewsDateDialog from '../src/components/news/edit/editNewsDateDialog.vue'
import EditNewsTimeDialog from '../src/components/news/edit/editNewsTimeDialog.vue'
import RegisterDialog from '../src/components/news/registration/registerDialog.vue'

Vue.use(Vuetify)
Vue.use(VueResource)
Vue.filter('date', DateFilter)
Vue.component('app-alert', AlertCmp)
Vue.component('app-edit-news-details-dialog', EditNewsDetailsDialog)
Vue.component('app-edit-news-date-dialog', EditNewsDateDialog)
Vue.component('app-edit-news-time-dialog', EditNewsTimeDialog)
Vue.component('app-news-register-dialog', RegisterDialog)

Vue.config.productionTip = false
// Vue.http.options.emulateJSON = true
// Vue.http.options.xhr = {withCredentials: true}
// Vue.http.headers.common['Content-Type'] = 'application/json'
// Vue.http.headers.common['Accept'] = 'application/json'
// Vue.http.headers.common['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin'
// Vue.http.headers.common['Access-Control-Allow-Origin'] = '*'

Vue.http.options.emulateJSON = true
Vue.http.headers.common['Content-Type'] = 'application/json'
Vue.http.headers.common['Accept'] = 'application/json, text/plain, */*'
Vue.http.headers.common['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin'


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  created () {
    firebase.initializeApp({
      apiKey: "AIzaSyAa8TmJHCh0aH-ykZtzIquhEFHHHV1M3Wc",
      authDomain: "online-study-89.firebaseapp.com",
      databaseURL: "https://online-study-89.firebaseio.com",
      projectId: "online-study-89",
      storageBucket: "gs://online-study-89.appspot.com"
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
        this.$store.dispatch('fetchUserData')
      }
    })
    this.$store.dispatch('loadNews')
  }
})
