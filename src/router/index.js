import Vue from 'vue'
import Router from 'vue-router'

import home from '@/components/home'
import newsList from '@/components/news/newsList'
import news from '@/components/news/news'
import createNews from '@/components/news/createNews'
import signup from '@/components/user/signup'
import signin from '@/components/user/signin'

import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/newsList',
      name: 'newsList',
      component: newsList
    },
    {
      path: '/news/createNews',
      name: 'createNews',
      component: createNews,
      beforeEnter: AuthGuard
    },
    {
      path: '/news/:id',
      name: 'news',
      props: true,
      component: news
    },
    {
      path: '/signup',
      name: 'signup',
      component: signup
    },
    {
      path: '/signin',
      name: 'signin',
      component: signin
    }
  ]
})
