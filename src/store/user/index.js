import * as firebase from 'firebase'
import Vue from 'vue'

import axios from 'axios';

const AUTH_BASIC_HEADERS = {
  'Accept': 'application/json',
  'Accept-Language': 'en_US',
  'Content-Type': 'application/json/x-www-form-urlencoded',
  'Access-Control-Allow-Headers':'Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin',
  'Access-Control-Allow-Origin': '*'
}

var instance = axios.create({
  baseURL: 'http://localhost:8000/api',
  timeout: 10000,
  headers: {
    'accept': 'application/json',
    'accept-language': 'en_US',
    'content-type': 'application/json'
  }
});

export default {
  state: {
    user: null,
  },
  mutations: {
    registerUserForNews (state, payload) {
      const id = payload.id
      if (state.user.registeredNews.findIndex(news => { news.id === id}) >= 0) {
        return
      }

      state.user.registeredNews.push(id)
      state.user.fbKeys[id] = payload.fbKey
    },
    unRegisterUserForNews (state, payload) {
      const registeredNews = state.user.registeredNews
      registeredNews.splice(registeredNews.findIndex(news => news.id === payload), 1)
      Reflect.deleteProperty(state.user.fbKeys, payload)
    },
    setUser (state, payload) {
      state.user = payload
    }
  },
  actions: {
    registerUserForNews ({commit, getters}, payload) {
      commit('setLoading', true)
      const user = getters.user
      firebase.database().ref('/users/' + user.id).child('/registrations/').push(payload).then(data => {
        commit('setLoading', false)
        commit('registerUserForNews', {
          id: payload,
          fbKey: data.key
        })
      }).catch(error => {
        console.log(error)
        commit('setLoading', false)
      })
    },
    unRegisterUserForNews ({commit, getters}, payload) {
      commit('setLoading', true)
      const user = getters.user
      if (!user.fbKeys) {
        commit('setLoading', false)
        return
      }
      const fbKey = user.fbKeys[payload]

      firebase.database().ref('/users/' + user.id + '/registrations/').child(fbKey).remove().then(() => {
        commit('setLoading', false)
        commit('unRegisterUserForNews', payload)
      }).catch(error => {
        console.log(error)
        commit('setLoading', false)
      })
    },
    signUserUp ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')

      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password).then(
        user => {
          commit('setLoading', false)
          const newUser = {
            id: user.uid,
            registeredNews: [],
            fbKeys: {}
          }
          // Reach out to fire base and store it
          commit('setUser', newUser)
        }
      ).catch (
        error => {
          commit('setLoading', false)
          commit('setError', error)
          console.log(error)
        }
      )
    },
    signUserIn ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')

      const parameters = { email: payload.email, password: payload.password }
      instance.post('http://localhost:8000/api/authentication', parameters, AUTH_BASIC_HEADERS).then( user => {
        commit('setLoading', false)
        const newUser = {
          id: user.uid,
          registeredNews: [],
          fbKeys: {}
        }
        // Reach out to fire base and store it
        commit('setUser', newUser)
      }).catch (error => {
          commit('setLoading', false)
          commit('setError', error)
          console.log(error)
      })
      // Authentication by using firebase
      /*firebase.auth().signInWithEmailAndPassword(payload.email, payload.password).then(
        user => {
          commit('setLoading', false)
          const newUser = {
            id: user.uid,
            registeredNews: [],
            fbKeys: {}
          }
          // Reach out to fire base and store it
          commit('setUser', newUser)
        }
      ).catch (
        error => {
          commit('setLoading', false)
          commit('setError', error)
          console.log(error)
        }
      )*/
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', {
        id: payload.uid,
        registeredNews: [],
        fbKeys: {}
      })
    },
    fetchUserData ({commit, getters}, payload) {
      commit('setLoading', true)
      firebase.database().ref('/users/' + getters.user.id + 'registrations').once('value').then(data => {
        const dataPairs = data.val()
        let registeredNews = []
        let swappedPairs = {}

        for (let key in dataPairs) {
          registeredNews.push(dataPairs[key])
          swappedPairs[registeredNews[key]] = key
        }
        const updatedUser = {
          id: getters.user.id,
          registeredNews: registeredNews,
          fbKeys: swappedPairs
        }
        commit('setLoading', false)
        commit('setUser', updatedUser)
      }).catch (
        error => {
          commit('setLoading', false)
          commit('setError', error)
          console.log(error)
        }
      )
    },
    login ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')


    },
    logout ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
    }
  },
  getters: {
    user (state) {
      return state.user
    }
  }
}
