import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    newsList: [
      // {
      //   id: 'afajfjadfaadfa323',
      //   title: 'Big Data',
      //   location: 'US',
      //   imageUrl: 'http://p.motionelements.com/stock-video/business/me2085800-big-data-hd-a0624.jpg',
      //   date: '10/2/2017',
      //   description: 'Big Data',
      //   creatorId: 'isLEJREDF2eZDIw2gv9GD1IoRlv2'
      // },
      // {
      //   id: 'aadsfhbkhlk1241',
      //   title: 'Machine Learning',
      //   location: 'US',
      //   imageUrl: 'https://juxt.pro/img/machine-human.jpg',
      //   date: '10/3/2017',
      //   description: 'Machine Learning',
      //   creatorId: 'isLEJREDF2eZDIw2gv9GD1IoRlv2'
      // },
      // {
      //   id: 'aadsfhbkhlk1242',
      //   title: 'Internet Of Things',
      //   location: 'US',
      //   imageUrl: 'https://pbs.twimg.com/media/CmEsnlPVEAA7Yec.jpg',
      //   date: '9/17/2017',
      //   description: 'Machine Learning',
      //   creatorId: 'isLEJREDF2eZDIw2gv9GD1IoRlv2'
      // }
    ],
    user: null,
    loading: false,
    error: null
  },
  mutations: {
    registerUserForNews (state, payload) {
      const id = payload.id
      if (state.user.registeredNews.findIndex(news => { news.id === id}) >= 0) {
        return
      }

      state.user.registeredNews.push(id)
      state.user.fbKeys[id] = payload.fbKey
    },
    unRegisterUserForNews (state, payload) {
      const registeredNews = state.user.registeredNews
      registeredNews.splice(registeredNews.findIndex(news => news.id === payload), 1)
      Reflect.deleteProperty(state.user.fbKeys, payload)
    },
    setLoadedNews (state, payload) {
      state.loadNews = payload
    },
    createNews (state, payload) {
      state.loadNews.push(payload)
    },
    updateNews (state, payload) {
      const news = state.loadNews.find(news => {
        return news.id === payload.id
      })

      if (payload.title ) {
        news.title = payload.title
      }
      if (payload.description ) {
        news.description = payload.description
      }
      if (payload.date ) {
        news.date = payload.date
      }
    },
    setUser (state, payload) {
      state.user = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    clearError (state) {
      state.error = null
    }
  },
  actions: {
    registerUserForNews ({commit, getters}, payload) {
      commit('setLoading', true)
      const user = getters.user
      firebase.database().ref('/users/' + user.id).child('/registrations/').push(payload).then(data => {
        commit('setLoading', false)
        commit('registerUserForNews', {
          id: payload,
          fbKey: data.key
        })
      }).catch(error => {
        console.log(error)
        commit('setLoading', false)
      })
    },
    unRegisterUserForNews ({commit, getters}, payload) {
      commit('setLoading', true)
      const user = getters.user
      if (!user.fbKeys) {
        commit('setLoading', false)
        return
      }
      const fbKey = user.fbKeys[payload]

      firebase.database().ref('/users/' + user.id + '/registrations/').child(fbKey).remove().then(() => {
        commit('setLoading', false)
        commit('unRegisterUserForNews', payload)
      }).catch(error => {
        console.log(error)
        commit('setLoading', false)
      })
    },
    loadNews ({commit}) {
      commit('setLoading', true)
      firebase.database().ref('news',).once('value').then((data) => {
        const newsList = this.getters.newsList
        const obj = data.val()

        for(let key in obj) {
          newsList.push({
            id: key,
            title: obj[key].title,
            location: obj[key].location,
            imageUrl: obj[key].imageUrl,
            date: obj[key].date,
            description: obj[key].description,
            creatorId: obj[key].creatorId
          })
        }

        commit('setLoadedNews', newsList)
        commit('setLoading', false)
      }).catch((error) => {
        console.log(error)
        commit('setLoading', false)
      })
    },
    createNews ({commit, getters}, payload) {
      const news = {
        title: payload.title,
        location: payload.location,
        description: payload.description,
        date: payload.date.toISOString(),
        creatorId: getters.user.id
      }
      let key
      let imageUrl
      firebase.database().ref('news').push(news).then((data) => {
        key = data.key
        return key
      }).then(key => {
        const fileName = payload.image.name
        const ext = fileName.slice(fileName.lastIndexOf('.'))
        return firebase.storage().ref('news/' + key + '.' + ext).put(payload.image)
      }).then(fileData => {
        imageUrl = fileData.metadata.downloadURLs[0]
        return firebase.database().ref('news').child(key).update({imageUrl: imageUrl})
      }).then(() => {
        commit('createNews', {
          ...news,
          imageUrl: imageUrl,
          id: key
        });
      }).catch((error) => {
        console.log(error)
      })
    },
    updateNews ({commit}, payload) {
      commit('setLoading', true)
      const updateObj = {}
      if (payload.title ) {
        updateObj.title = payload.title
      }
      if (payload.description ) {
        updateObj.description = payload.description
      }
      if (payload.date ) {
        updateObj.date = payload.date
      }

      firebase.database().ref('news').child(payload.id).update(updateObj).then(() => {
        commit('setLoading', false)
        commit('updateNews', payload)
      }).catch(error => {
        console.log(error)
        commit('setLoading', false)
      })
    },
    signUserUp ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')

      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password).then(
        user => {
          commit('setLoading', false)
          const newUser = {
            id: user.uid,
            registeredNews: [],
            fbKeys: {}
          }
          // Reach out to fire base and store it
          commit('setUser', newUser)
        }
      ).catch (
        error => {
          commit('setLoading', false)
          commit('setError', error)
          console.log(error)
        }
      )
    },
    signUserIn ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')

      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password).then(
        user => {
          commit('setLoading', false)
          const newUser = {
            id: user.uid,
            registeredNews: [],
            fbKeys: {}
          }
          // Reach out to fire base and store it
          commit('setUser', newUser)
        }
      ).catch (
        error => {
          commit('setLoading', false)
          commit('setError', error)
          console.log(error)
        }
      )
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', {
        id: payload.uid,
        registeredNews: [],
        fbKeys: {}
      })
    },
    fetchUserData ({commit, getters}, payload) {
      commit('setLoading', true)
      firebase.database().ref('/users/' + getters.user.id + 'registrations').once('value').then(data => {
        const dataPairs = data.val()
        let registeredNews = []
        let swappedPairs = {}

        for (let key in dataPairs) {
          registeredNews.push(dataPairs[key])
          swappedPairs[registeredNews[key]] = key
        }
        const updatedUser = {
          id: getters.user.id,
          registeredNews: registeredNews,
          fbKeys: swappedPairs
        }
        commit('setLoading', false)
        commit('setUser', updatedUser)
      }).catch (
        error => {
          commit('setLoading', false)
          commit('setError', error)
          console.log(error)
        }
      )
    },
    logout ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
    },
    clearError ({commit}) {
      commit('clearError')
    }
  },
  getters: {
    newsList (state) {
      return state.newsList.sort((meetupA, meetupB) => {
        return meetupA.date >= meetupB.date
      })
    },
    featuredMeetups (state, getters) {
      return getters.newsList.slice(0, 5)
    },
    loadedNews (state) {
      return (newsId) => {
        return state.newsList.find((news) => {
          return news.id === newsId
        })
      }
    },
    user (state) {
      return state.user
    },
    loading (state) {
      return state.loading
    },
    error (state) {
      return state.error
    }
  }
})
