import Vue from 'vue'
import Vuex from 'vuex'

import news from './news'
import user from './user'
import shared from './shared'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    news: news,
    user: user,
    shared: shared
  }
})
