import * as firebase from 'firebase'

export default {
  state: {
    newsList: []
  },
  mutations: {
    setLoadedNews (state, payload) {
      state.loadNews = payload
    },
    createNews (state, payload) {
      state.loadNews.push(payload)
    },
    updateNews (state, payload) {
      const news = state.loadNews.find(news => {
        return news.id === payload.id
      })

      if (payload.title) {
        news.title = payload.title
      }
      if (payload.description) {
        news.description = payload.description
      }
      if (payload.date) {
        news.date = payload.date
      }
    }
  },
  actions: {
    loadNews ({commit}) {
      commit('setLoading', true)
      firebase.database().ref('news',).once('value').then((data) => {
        const newsList = this.getters.newsList
        const obj = data.val()

        for(let key in obj) {
          newsList.push({
            id: key,
            title: obj[key].title,
            location: obj[key].location,
            imageUrl: obj[key].imageUrl,
            date: obj[key].date,
            description: obj[key].description,
            creatorId: obj[key].creatorId
          })
        }

        commit('setLoadedNews', newsList)
        commit('setLoading', false)
      }).catch((error) => {
        console.log(error)
        commit('setLoading', false)
      })
    },
    createNews ({commit, getters}, payload) {
      const news = {
        title: payload.title,
        location: payload.location,
        description: payload.description,
        date: payload.date.toISOString(),
        creatorId: getters.user.id
      }
      let key
      let imageUrl
      firebase.database().ref('news').push(news).then((data) => {
        key = data.key
        return key
      }).then(key => {
        const fileName = payload.image.name
        const ext = fileName.slice(fileName.lastIndexOf('.'))
        return firebase.storage().ref('news/' + key + '.' + ext).put(payload.image)
      }).then(fileData => {
        imageUrl = fileData.metadata.downloadURLs[0]
        return firebase.database().ref('news').child(key).update({imageUrl: imageUrl})
      }).then(() => {
        commit('createNews', {
          ...news,
          imageUrl: imageUrl,
          id: key
        });
      }).catch((error) => {
        console.log(error)
      })
    },
    updateNews ({commit}, payload) {
      commit('setLoading', true)
      const updateObj = {}
      if (payload.title ) {
        updateObj.title = payload.title
      }
      if (payload.description ) {
        updateObj.description = payload.description
      }
      if (payload.date ) {
        updateObj.date = payload.date
      }

      firebase.database().ref('news').child(payload.id).update(updateObj).then(() => {
        commit('setLoading', false)
        commit('updateNews', payload)
      }).catch(error => {
        console.log(error)
        commit('setLoading', false)
      })
    }
  },
  getters: {
    newsList (state) {
      return state.newsList.sort((meetupA, meetupB) => {
        return meetupA.date >= meetupB.date
      })
    },
    loadedNews (state) {
      return (newsId) => {
        return state.newsList.find((news) => {
          return news.id === newsId
        })
      }
    }
  }
}
